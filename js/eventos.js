function change(){
    let mOrigen = document.getElementById('monedaOrigen').value;
    let mDestino = document.getElementById('monedaDestino');
    let opciones = "";

    if(mOrigen == 'dolarE'){
        opciones = '<option value="peso">Peso Mexicanos</option><option value="dolarC">Dólar Canadiense</option><option value="euro">Euro</option>';
    }else if(mOrigen == 'peso'){
        opciones = '<option value="dolarE">Dólar Estadounidense</option><option value="dolarC">Dólar Canadiense</option><option value="euro">Euro</option>';
    }else if(mOrigen == 'dolarC'){
        opciones = '<option value="dolarE">Dólar Estadounidense</option><option value="peso">Peso Mexicanos</option><option value="euro">Euro</option>';
    }else if(mOrigen == 'euro'){
        opciones = '<option value="dolarE">Dólar Estadounidense</option><option value="peso">Peso Mexicanos</option><option value="dolarC">Dólar Canadiense</option>';
    }else if(mOrigen == 'inx'){
        opciones = '<option value="inx">Moneda destino</option>';
    }

    mDestino.innerHTML = opciones;

}

function calcular(){
    let cantidad = document.getElementById('cantidad').value;
    let origen = document.getElementById('monedaOrigen').value;
    let destino = document.getElementById('monedaDestino').value;

    var total = 0;

    // Conversión Dólar Estadounidense como inicial
    if(origen == 'dolarE' && destino == 'peso'){
        total = cantidad * 19.85; 
    }else if(origen == 'dolarE' && destino == 'dolarC'){
        total = cantidad * 1.35;
    }else if(origen == 'dolarE' && destino == 'euro'){
        total = cantidad * 0.99;
    }
    
    // Conversión Peso mexicano como inicial
    if(origen == 'peso' && destino == 'dolarE'){
        total = cantidad / 19.85; 
    }else if(origen == 'peso' && destino == 'dolarC'){
        total = cantidad / 19.85;
        total = total * 1.35;
    }else if(origen == 'peso' && destino == 'euro'){
        total = cantidad / 19.85; 
        total = total * 0.99;
    }

    // Conversión Dólar canadiense como inicial
    if(origen == 'dolarC' && destino == 'dolarE'){
        total = cantidad / 1.35; 
    }else if(origen == 'dolarC' && destino == 'peso'){
        total = total / 1.35;
        total = total * 19.85;
    }else if(origen == 'dolarC' && destino == 'euro'){
        total = cantidad / 1.35; 
        total = total * 0.99;
    }

    // Conversión Euro como inicial
    if(origen == 'euro' && destino == 'dolarE'){
        total = cantidad / 0.99; 
    }else if(origen == 'euro' && destino == 'peso'){
        total = total / 0.99;
        total = total * 19.85;
    }else if(origen == 'euro' && destino == 'dolarC'){
        total = cantidad / 0.99; 
        total = total * 1.35;
    }

    let resSubtotal = total;
    let resComision = resSubtotal * 0.03;
    let resTotalPagar = resSubtotal + resComision;

    let txtSubtotal = document.getElementById('subtotal');
    let txtComision = document.getElementById('comision');
    let txtTotalPagar = document.getElementById('totalPagar');

    // Mostrar información
    txtSubtotal.innerHTML = txtSubtotal.setAttribute("value", resSubtotal);
    txtComision.innerHTML = txtComision.setAttribute("value", resComision);
    txtTotalPagar.innerHTML = txtTotalPagar.setAttribute("value", resTotalPagar);
}

// Variables globales que se van a usar para guardar el total de los registros
var subtotalFinal = 0;
var totalComisionFinal = 0;
var totalPagarFinal = 0;

function registro(){
    let cantidad = document.getElementById('cantidad').value;
    let origen = document.getElementById('monedaOrigen').value;
    let destino = document.getElementById('monedaDestino').value;

    var total = 0;

    // Conversión Dólar Estadounidense como inicial
    if(origen == 'dolarE' && destino == 'peso'){
        total = cantidad * 19.85; 
    }else if(origen == 'dolarE' && destino == 'dolarC'){
        total = cantidad * 1.35;
    }else if(origen == 'dolarE' && destino == 'euro'){
        total = cantidad * 0.99;
    }
    
    // Conversión Peso mexicano como inicial
    if(origen == 'peso' && destino == 'dolarE'){
        total = cantidad / 19.85; 
    }else if(origen == 'peso' && destino == 'dolarC'){
        total = cantidad / 19.85;
        total = total * 1.35;
    }else if(origen == 'peso' && destino == 'euro'){
        total = cantidad / 19.85; 
        total = total * 0.99;
    }

    // Conversión Dólar canadiense como inicial
    if(origen == 'dolarC' && destino == 'dolarE'){
        total = cantidad / 1.35; 
    }else if(origen == 'dolarC' && destino == 'peso'){
        total = total / 1.35;
        total = total * 19.85;
    }else if(origen == 'dolarC' && destino == 'euro'){
        total = cantidad / 1.35; 
        total = total * 0.99;
    }

    // Conversión Euro como inicial
    if(origen == 'euro' && destino == 'dolarE'){
        total = cantidad / 0.99; 
    }else if(origen == 'euro' && destino == 'peso'){
        total = total / 0.99;
        total = total * 19.85;
    }else if(origen == 'euro' && destino == 'dolarC'){
        total = cantidad / 0.99; 
        total = total * 1.35;
    }

    // Con esto conseguir los label donde se van a ingresar los datos
    let lblCantidad = document.getElementById('lblCantidad');
    let lblMonedaOrigen = document.getElementById('lblMonOrigen');
    let lblMonedaDestino = document.getElementById('lblMonDestino');
    let lblSubtotal = document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = document.getElementById('lblTotalPagar');

    let resSubtotal = total;
    let resComision = resSubtotal * 0.03;
    let resTotalPagar = resSubtotal + resComision;

    // Moneda de origen
    var monedaOrigenT;
    if (origen == 'peso') {
        monedaOrigenT = "Peso mexicano a";
    }else if (origen == 'dolarE') {
        monedaOrigenT = "Dólar estadounidense a";
    }else if (origen == 'dolarC') {
        monedaOrigenT = "Dólar canadiense a";
    }else if (origen == 'euro') {
        monedaOrigenT = "Euro a";
    }

    // Moneda de destino
    var monedaDestinoT;
    if (destino == 'peso') {
        monedaDestinoT = "Peso mexicano";
    }else if (destino == 'dolarE') {
        monedaDestinoT = "Dólar estadounidense";
    }else if (destino == 'dolarC') {
        monedaDestinoT = "Dólar canadiense";
    }else if (destino == 'euro') {
        monedaDestinoT = "Euro";
    }

    // Mostrar los labels seleccionados
    lblCantidad.innerHTML = lblCantidad.innerHTML + cantidad + "<br>";
    lblMonedaOrigen.innerHTML = lblMonedaOrigen.innerHTML + monedaOrigenT + "<br>";
    lblMonedaDestino.innerHTML = lblMonedaDestino.innerHTML + monedaDestinoT + "<br>";
    lblSubtotal.innerHTML = lblSubtotal.innerHTML + resSubtotal + "<br>";
    lblTotalComision.innerHTML = lblTotalComision.innerHTML + resComision + "<br>";
    lblTotalPagar.innerHTML = lblTotalPagar.innerHTML + resTotalPagar + "<br>";

    // Conseguir la suma general de los registros
    let regSubtotal = document.getElementById('subtotalGral');
    let regTotalComision = document.getElementById('totComisionGral');
    let regTotalPagar = document.getElementById('totPagarGral');

    subtotalFinal = subtotalFinal + resSubtotal;

    regSubtotal.innerText = ""
    regSubtotal.innerText = regSubtotal.innerText + subtotalFinal;

    totalComisionFinal = totalComisionFinal + resComision;

    regTotalComision.innerText = ""
    regTotalComision.innerText = regTotalComision.innerText + totalComisionFinal;

    totalPagarFinal = totalPagarFinal + resTotalPagar;

    regTotalPagar.innerText = ""
    regTotalPagar.innerText = regTotalPagar.innerText + totalPagarFinal;
}

function borrarRegistros(){
    let lblCantidad = document.getElementById('lblCantidad');
    let lblMonedaOrigen = document.getElementById('lblMonOrigen');
    let lblMonedaDestino = document.getElementById('lblMonDestino');
    let lblSubtotal = document.getElementById('lblSubtotal');
    let lblTotalComision = document.getElementById('lblTotalComision');
    let lblTotalPagar = document.getElementById('lblTotalPagar');

    lblCantidad.innerHTML =""
    lblMonedaOrigen.innerHTML =""
    lblMonedaDestino.innerHTML =""
    lblSubtotal.innerHTML =""
    lblTotalComision.innerHTML =""
    lblTotalPagar.innerHTML =""

    let regSubtotal = document.getElementById('subtotalGral');
    let regTotalComision = document.getElementById('totComisionGral');
    let regTotalPagar = document.getElementById('totPagarGral');

    regSubtotal.innerText = "---"
    subtotalFinal = 0;

    regTotalComision.innerText = "---"
    totalComisionFinal = 0;

    regTotalPagar.innerText = "---"
    totalPagarFinal = 0;
}